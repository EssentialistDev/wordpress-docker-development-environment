<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pwd' );

/** MySQL database username */
define( 'DB_USER', 'pwd' );

/** MySQL database password */
define( 'DB_PASSWORD', 'pwd' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']7[$%4Hq*VFOUFm%}c$T[SdRhXuo,dC.9wT?yk`30=k^wH%<0n/^dz(JpL4X_WKB' );
define( 'SECURE_AUTH_KEY',  'deCc^1}&eU68xP!|ZWN|=0>?/StmmXVL?,Xa|O;W~P$dx*JA2^Ag7B[)rP4wz1O`' );
define( 'LOGGED_IN_KEY',    '{YS).?-qQ@x%(9cHkR% 1@.Xj^Q_R>$Suy6qMIT12$nS,_xl$<VLt]FvKN){(+p/' );
define( 'NONCE_KEY',        '~PI7j<_n5H{p)+.ZY*?zs,av5GH.aln`:tI].=7#b<P|`9gjgQ8_{42sM`dJn7Z6' );
define( 'AUTH_SALT',        'AjBakI|R*$dRHl>$2W@yy*(]gUMVwKSBHQe9XurMq l8n92}t&8zk:OcPAq:jp56' );
define( 'SECURE_AUTH_SALT', 'K[dAk7tt;.$o]Evw4;Mq$cdI:;?_.KR r4.}W*pFK_;C?JJ;~vP(*lx,[g;GF?aH' );
define( 'LOGGED_IN_SALT',   'i<{;KG7BrgME, uw~[ddz7fet:B-:><+$OM2 M/BZ!@1jEW~WYWezR%4c@?!eWwS' );
define( 'NONCE_SALT',       'l#U6!.jk:Ylighe5NqUj!{021$w2hOIOb#ccdW/Z$$`DhJ[QMYD-p@p+fJ[9zO,V' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
