# Local Dev Environment for WordPress with Docker

This is a local developer environment for WordPress, where we use Docker to provide our web server (NGINX with PHP) and database (MariaDB). All of the settings for these are configured in the `docker-compose.yml` file.

WordPress is added as a Git submodule, because Docker creates a web server for each of our services (containers). The web server and database requires this, but the WordPress repository does not, as it now has everything it needs to run so another web server would be unnecessary.

### To use

- Install Docker via Homebrew on Mac: `brew install --cask docker`
- Start docker by searching for the app (once installed) and running it.
- Clone this repo.
- Change your MariaDB database credentials in the "docker-compose.yml" file. I.e. the database name, passwords and username etc.
- Go inside the repo and run `docker compose up`.
- Go to `https://localhost:8080` and go through the WordPress setup wizard. Write in the same database setup credentials that you setup in the compose file.


### To develop a theme

If you want to develop a theme using this developer environment, create a new folder/repo in a different place for your theme, totally outside of this one. Then use a symbolic link to link your theme into your wordpress directory inside of this repository. This way your developer environment will be seperate from your theme files. Reusable.
